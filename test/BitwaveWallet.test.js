/* eslint-disable max-len */
const crypto = require('crypto');

const MultiSend = artifacts.require('./BitwaveMultiSend.sol');
const ERC20 = artifacts.require('./TestMintableToken.sol');

const BigNumber = require("bignumber.js");

const bn = BigNumber;

async function getBalance(address) {
    const res = await web3.eth.getBalance(address);
    if(res === undefined) {
        return new BigNumber(0);
    } else {
        return new BigNumber(res);
    }
}

function getBalances(addresses) {
  return Promise.all(addresses.map(getBalance));
}

async function getTokenBalance(token, address) {
  const bal = await token.balanceOf.call(address);
  return new BigNumber(bal);
}

function getTokenBalances(token, addresses) {
  return Promise.all(addresses.map(a => getTokenBalance(token, a)));
}


function randomAddress() {
  return `0x${crypto.randomBytes(20).toString('hex')}`;
}

function randomAddresses(n) {
  return Array(n).fill().map(randomAddress);
}

contract('BitwaveMultiSend', accounts => {
  const from = accounts[0];
  let multisend;
  before(async () => {
    multisend = await MultiSend.deployed({from});
  });
  describe('sendEth', () => {
    it('sends to a few users', async () => {
      const amounts = [1, 2, 3];
      const value = amounts.reduce((n, a) => n + a, 0);
      const recipients = randomAddresses(amounts.length);
      const before = await getBalances(recipients);
      await multisend.sendEth(recipients, amounts, { value });
      const after = await getBalances(recipients);
      amounts.forEach((a, i) => {
        assert.deepEqual(after[i],  before[i].plus(amounts[i]));
      });
      const addressBalance = await getBalance(multisend.address);
      assert.equal(addressBalance.toNumber(), 0);
      assert.equal(await multisend.owner(), from);
    });
    it('sends to a lot of users', async () => {
      const amounts = Array.from(Array(100).keys());
      const value = amounts.reduce((n, a) => n + a, 0);
      const recipients = randomAddresses(amounts.length);
      const before = await getBalances(recipients);
      await multisend.sendEth(recipients, amounts, { value });
      const after = await getBalances(recipients);
      amounts.forEach((a, i) => {
        const aft = after[i].toNumber();
        const bef = before[i].toNumber();
        const amt = amounts[i];

        assert.equal(aft, bef + amt);
      });

      assert.deepEqual(await getBalance(multisend.address), bn(0));
    });
    it('returns to sender when sending more value', async () => {
      const amounts = [1, 2, 3, 20];
      const actualValue = amounts.reduce((n, a) => n + a, 0);
      const value = 200;
      const senderBalance = await getBalance(from);
      const recipients = randomAddresses(amounts.length);
      const before = await getBalances(recipients);
      const gasPrice = 4e9;
      const { receipt: { gasUsed } } = await multisend.sendEth(recipients, amounts, { value, gasPrice });
      const gasCost = gasPrice * gasUsed;
      const after = await getBalances(recipients);
      amounts.forEach((a, i) => assert.deepEqual(after[i], before[i].plus(amounts[i])));
      assert.deepEqual(await getBalance(multisend.address), bn(0));
      assert.deepEqual(await getBalance(from), senderBalance.minus(gasCost).minus(actualValue));
    });
    it('throws when sending to too many users (out of gas)', async () => {
      const amounts = Array.from(Array(200).keys());
      const value = amounts.reduce((n, a) => n + a, 0);
      const recipients = randomAddresses(amounts.length);
      const before = await getBalances(recipients);
      let thrown = false;
      try {
        await multisend.sendEth(recipients, amounts, { value });
      } catch (e) {
        thrown = true;
      }
      assert.equal(thrown, true);
      const after = await getBalances(recipients);
      amounts.forEach((a, i) => assert.deepEqual(after[i], before[i]));
      assert.deepEqual(await getBalance(multisend.address), bn(0));
    });

    it('throws when sending to too many users (greater than uint8)', async () => {
      const amounts = Array.from(Array(256).keys());
      const value = amounts.reduce((n, a) => n + a, 0);
      const recipients = randomAddresses(amounts.length);
      const before = await getBalances(recipients);
      let thrown = false;
      try {
        await multisend.sendEth(recipients, amounts, { value });
      } catch (e) {
        thrown = true;
      }
      assert.equal(thrown, true);
      const after = await getBalances(recipients);
      amounts.forEach((a, i) => assert.deepEqual(after[i], before[i]));
      assert.deepEqual(await getBalance(multisend.address), bn(0));
    });

    it('throws when not sending enough value', async () => {
      const amounts = [1, 2, 3];
      const recipients = randomAddresses(amounts.length);
      const before = await getBalances(recipients);
      let thrown = false;
      try {
        await multisend.sendEth(recipients, amounts, { value: 3 });
      } catch (e) {
        thrown = true;
      }
      assert.equal(thrown, true);
      const after = await getBalances(recipients);
      amounts.forEach((a, i) => assert.deepEqual(after[i], before[i]));
      assert.deepEqual(await getBalance(multisend.address), bn(0));
    });

    it('throws when being sent by the wrong user', async () => {
      const amounts = [1, 2, 3];
      const value = amounts.reduce((n, a) => n + a, 0);
      const recipients = randomAddresses(amounts.length);
      const before = await getBalances(recipients);

      let thrown = false;
      try {
        const res = await multisend.sendEth(recipients, amounts, { from: accounts[1], value });
      } catch(e) {
        thrown = true;
      }

      assert.equal(thrown, true);
      const after = await getBalances(recipients);
      amounts.forEach((a, i) => assert.deepEqual(after[i], before[i]));
      assert.deepEqual(await getBalance(multisend.address), bn(0));
    });
  });

  describe('sendErc20', () => {
           const initialTokens = 5000;
           let erc20;
           beforeEach(async () => {
             erc20 = await ERC20.new();
             await erc20.mint(from, initialTokens);
           });
           it('sends to a few users', async () => {
             const amounts = [1, 2, 3];
             const value = amounts.reduce((n, a) => n + a, 0);
             const recipients = randomAddresses(amounts.length);
             const before = await getTokenBalances(erc20, recipients);
             await erc20.approve(multisend.address, value);
             await multisend.sendErc20(erc20.address, recipients, amounts);
             const after = await getTokenBalances(erc20, recipients);
             amounts.forEach((a, i) => assert.deepEqual(after[i], before[i].plus(amounts[i])));

             const bal = await getTokenBalance(erc20, from);
             assert.deepEqual(bal, bn(initialTokens - value));
           });
           it('sends to a lot of users', async () => {
             const amounts = Array.from(Array(100).keys());
             const value = amounts.reduce((n, a) => n + a, 0);
             const recipients = randomAddresses(amounts.length);
             const before = await getTokenBalances(erc20, recipients);
             await erc20.approve(multisend.address, value);
             await multisend.sendErc20(erc20.address, recipients, amounts);
             const after = await getTokenBalances(erc20, recipients);
             amounts.forEach((a, i) => assert.deepEqual(after[i], before[i].plus(amounts[i])));
             assert.deepEqual(await getTokenBalance(erc20, from), bn(initialTokens - value));
           });
           it('throws when sending to too many users (out of gas)', async () => {
             const amounts = Array.from(Array(200).keys());
             const value = amounts.reduce((n, a) => n + a, 0);
             const recipients = randomAddresses(amounts.length);
             const before = await getTokenBalances(erc20, recipients);
             await erc20.approve(multisend.address, value);
             let thrown = false;
             try {
               await multisend.sendErc20(erc20.address, recipients, amounts);
             } catch (e) {
               thrown = true;
             }
             assert.equal(thrown, true);
             const after = await getBalances(recipients);
             amounts.forEach((a, i) => assert.deepEqual(after[i], before[i]));
             assert.deepEqual(await getTokenBalance(erc20, from), bn(initialTokens));
           });
           it('throws when sending to too many users (greater than uint8)', async () => {
             const amounts = Array.from(Array(256).keys());
             const value = amounts.reduce((n, a) => n + a, 0);
             const recipients = randomAddresses(amounts.length);
             const before = await getTokenBalances(erc20, recipients);
             await erc20.approve(multisend.address, value);
             let thrown = false;
             try {
               await multisend.sendErc20(erc20.address, recipients, amounts);
             } catch (e) {
               thrown = true;
             }
             assert.equal(thrown, true);
             const after = await getBalances(recipients);
             amounts.forEach((a, i) => assert.deepEqual(after[i], before[i]));
             assert.deepEqual(await getTokenBalance(erc20, from), bn(initialTokens));
           });
           it('throws when not approved enough', async () => {
             const amounts = [1, 2, 3];
             const recipients = randomAddresses(amounts.length);
             const before = await getTokenBalances(erc20, recipients);
             await erc20.approve(multisend.address, 2);
             let thrown = false;
             try {
               await multisend.sendErc20(erc20.address, recipients, amounts);
             } catch (e) {
               thrown = true;
             }
             assert.equal(thrown, true);
             const after = await getBalances(recipients);
             amounts.forEach((a, i) => assert.deepEqual(after[i], before[i]));
             assert.deepEqual(await getTokenBalance(erc20, from), bn(initialTokens));
           });

           it('throws when sent by a non owner', async () => {
                const amounts = [1, 2, 3];
                const value = amounts.reduce((n, a) => n + a, 0);
                const recipients = randomAddresses(amounts.length);
                const before = await getTokenBalances(erc20, recipients);
                await erc20.approve(multisend.address, value);

                let thrown = false;

                try {
                    await multisend.sendErc20(erc20.address, recipients, amounts, {from: accounts[1]});
                } catch(e) {
                    thrown = true;
                }

                assert.equal(thrown, true);
                const after = await getBalances(recipients);
                amounts.forEach((a, i) => assert.deepEqual(after[i], before[i]));
                assert.deepEqual(await getTokenBalance(erc20, from), bn(initialTokens));
              });
         });
});
